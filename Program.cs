﻿internal class Program
{
    private static void Main(string[] args)
    {
        double result = GetValue("Введите первое число: ");
        while (true)
        {
            Console.Write("Операция: ");
            string cmd = Console.ReadLine();
            if (cmd == "exit")
                break;

            int y = GetValue("Введите значение: ");
            result = GetResult(result, y, cmd);

            Console.WriteLine(string.Format($"Результат: {result}"));
        }
    }

    private static bool AskContinue()
    {
        Console.WriteLine("Хотите продолжить?");
        var status = Console.ReadLine();
        if (status == "y" || status == "Y")
            return true;

        return false;
    }

    private static double GetMax(double x, int y)
    {
        double max = x;
        if (x < y)
            max = y;
        return max;
    }

    private static double GetMin(double x, int y)
    {
        double min = 0;
        if (x < y)
            min = x;
        else
            min = y;
        return min;
    }

    private static double GetResult(double x, int y, string cmd)
    {
        double result = x;
        switch (cmd)
        {
            case "+":
                result = x + y;
                break;
            case "-":
                result = x - y;
                break;
            case "*":
                result = x * y;
                break;
            case "/":
                result = (double)x / y;
                break;
            case "min":
                result = GetMin(x, y);
                break;
            case "max":
                result = GetMax(x, y);
                break;
        }

        return result;
    }

    private static int GetValue(string message)
    {
        Console.Write(message);
        string str = Console.ReadLine();
        int x = 0;
        while (!int.TryParse(str, out x))
        {
            Console.WriteLine("Неверный формат. Повторите ввод");
            str = Console.ReadLine();
        }
        return x;
    }
}